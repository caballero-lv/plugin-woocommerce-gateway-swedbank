=== WooCommerce Swedbank gateway v3 ===
Contributors: Darius Augaitis
Tags: Gateway, woocommerce, Swedbank, Card payment, Credit card, Debit Card,
Requires at least: 3.9
Tested up to: 5.6
Stable tag: 3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Swedbank gateway for bank transfer and Debit/Credit Card payment support for WooCommerce.

== Description ==

Swedbank gateway for bank transfer and Debit/Credit Card payment support for WooCommerce.

== Changelog ==

= 3.0 =
* Launch new product!
